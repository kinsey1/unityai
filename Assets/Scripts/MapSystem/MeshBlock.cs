﻿using UnityEngine;
using System.Collections.Generic;


public class MeshBlock : MonoBehaviour {

    public Vector3[] Vertices;
    private Vector2[] UV;
    private int[] Triangles;
    private Vector3[] normals;

    public Texture2D texture;
    public MeshFilter meshFilter;
    public MeshCollider meshCollider;
    public Renderer meshRenderer;

    MultiMeshManager meshManager;


    private Vector3 position;

    private float spacing;

    Color[] colors;

    private int
        length,
        triIdx;
    

    /// <summary>
    /// Initializes the smaller mesh block. this method handles setting up 
    /// Vertices, trianlges and the texture data.
    /// </summary>
    /// <param name="spacing">Spacing between vertices</param>
    /// <param name="length">length of a side</param>
    public void Initialize(float spacing, int length, MultiMeshManager meshManager)
    {
        this.length = length;
        this.spacing = spacing;

        triIdx = 0;
        texture = new Texture2D(length, length);
        Vertices = new Vector3[length*length];
        UV = new Vector2[Vertices.Length];
        Triangles = new int[(length - 1) * (length - 1) * 6];
        colors = new Color[Vertices.Length];
        int idx = 0;

        //Setting vertices, triangles and UV array
        for(int x = 0; x < length; x++) {
            for(int y = 0; y < length; y++) {


                if (y < length - 1 && x < length - 1)
                {
                    genTri(idx, idx + length + 1, idx + length);
                    genTri(idx + length + 1, idx, idx + 1);
                }

                UV[idx] = new Vector2(x / ((float)length), y / ((float)length));
                //UV[idx] = new Vector2(x/meshManager.vertexSpacing, y / meshManager.vertexSpacing);
                Vertices[idx] = new Vector3(x * spacing, 0, y * spacing);
                //UV[idx] = new Vector2(Vertices[idx].x, Vertices[idx].z);
                idx++;

            }
        }

       
    }

    /// <summary>
    /// sets all the relevant mesh data
    /// </summary>
    public void updateMesh()
    {
        Mesh mesh = new Mesh();
        
        mesh.vertices = Vertices;
        mesh.triangles = Triangles;
        mesh.uv = UV;
        
        meshCollider.sharedMesh = mesh;
        meshRenderer.material.mainTexture = texture;
        meshFilter.sharedMesh = mesh;
        mesh.RecalculateNormals();
        normals = meshFilter.sharedMesh.normals;
        
    }



    /// <summary>
    /// generates a triangle based off indexes. v1,v2,v3 are 
    /// indexes from the vertices array
    /// </summary>
    /// <param name="v1"></param>
    /// <param name="v2"></param>
    /// <param name="v3"></param>
    private void genTri(int v1, int v2, int v3)
    {
        Triangles[triIdx] = v1;
        Triangles[triIdx + 1] = v2;
        Triangles[triIdx + 2] = v3;
        triIdx += 3;
    }


    /// <summary>
    /// creates a texture (for individual mesh block) and applies it
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <param name="colors"></param>
    public void createTexture(float min, float max, List<Color> colors, List<Color> colors2) {

        for (int y = 0; y < length; y++)
        {
            for (int x = 0; x < length; x++)
            {

                float height = getVertexHeight(y,x);



                int idx = Mathf.FloorToInt(Mathf.InverseLerp(min, max, height) * colors.Count);
                if (idx >= colors.Count)
                {
                    idx = colors.Count - 1;
                }
                float rand = Random.Range(0f, 1f);
                if (rand > .2)
                {
                    texture.SetPixel(x, y, colors[idx]);
                }else
                {
                    texture.SetPixel(x, y, colors2[idx]);
                }
                
                
               


            }
        }
        //meshRenderer.material.mainTextureScale = new Vector2(0.001f, 0.001f);
        meshRenderer.material.mainTextureOffset = new Vector2(1.0f/(2*length), 1.0f/(2*length));
        texture.filterMode = FilterMode.Point;
        texture.Apply();

        
    }


    /// <summary>
    /// used for easily setting a height in the vertices array.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="val"></param>
    public void setVertexHeight(int x, int y, float val)
    {
        Vertices[y * length + x].y = val;
    }

    public float getVertexHeight(int x, int y)
    {

        return Vertices[y * length + x].y;
    }


    public void setNormalHeight(int y, int x, Vector3 val)
    {
        normals[y * length + x] = val;
    }

    public Vector3 getNormal(int y, int x)
    {
        return normals[y * length + x];
    }
   

    public void applyNormals()
    {
        meshFilter.sharedMesh.normals = normals;
    }
}
