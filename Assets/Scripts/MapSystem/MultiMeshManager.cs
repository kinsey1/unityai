﻿using UnityEngine;
using System.Collections.Generic;
using System;


/// <summary>
/// Class which handles the multiple meshes. The overall terrain is made up of multiple smaller 
/// meshes (due to unitys mesh restrictions)
/// </summary>
public class MultiMeshManager : MonoBehaviour {

    public GameObject block;

    public GameObject[,] meshs;
    public HeightMap heightmap;

    private MeshBlock[,] blocks;
    public Grid grid;
    public WorldManager worldManager;

    public List<Color> colors = new List<Color>();
    public List<Color> colors2 = new List<Color>();

    public Water water;


    

    /// <summary>
    /// the total mesh is comprised of multiple smaller meshes. each inner mesh is called
    ///  a block. 
    /// </summary>
    [Header("Terrain variables")]

    public int numBlocks;
    public int numBlockVertices;


    private int
        //total mesh size, = blockmeshsize* numblockwidth
        totalMeshVertices;

    public float
        vertexSpacing;

    [Space(20)]

    [Header("HeightMap Variables")]
    public int scalar;
    public float freq;
    public int numOctaves;
    public float redis;
    public bool island;
    public float islandPower;
    public float islb;
    private float mapMax,mapMin;
    

    


    void Start()
    {
        
        Game.meshManager = this;
        
        totalMeshVertices = numBlockVertices * numBlocks;
        mapMax = (totalMeshVertices * vertexSpacing)/2;
        mapMin = -(totalMeshVertices * vertexSpacing) / 2;
        water.Initialize();
        //Debug.Log(mapMax);
        //Debug.Log(mapMin);
        blocks = new MeshBlock[numBlocks, numBlocks];
        heightmap = new HeightMap(totalMeshVertices, scalar, freq, numOctaves, redis, island, islandPower, islb, false);

        Game.length = totalMeshVertices;
        //Debug.Log(Game.length);
        //Debug.Log(Game.getHeightFromWorld(new Vector3(0, 0, 0)));


        generateBlocks();
        setHeightMap(heightmap);


        for (int y = 0; y < numBlocks; y++)
        {
            for (int x = 0; x < numBlocks; x++)
            {
                blocks[x, y].updateMesh();
                blocks[x, y].createTexture(heightmap.getMinHeight(), heightmap.getMaxHeight(), colors, colors2);
            }
        }

        Debug.Log("MESH LOADING DONE");
        generateNodes();
        worldManager.Initialize();


    }

    







    /// <summary>
    /// Instantiates the individual mesh objects and updates the blocks array
    /// with all the meshblocks
    /// </summary>
    public void generateBlocks()
    {

        //instantiates the block game objects
        for (int y = 0; y < numBlocks; y++) {
            for(int x = 0; x < numBlocks; x++) {

                GameObject temp  = Instantiate(block,
                    //x 
                    new Vector3(vertexSpacing*x*(numBlockVertices-1) - (totalMeshVertices/2)*vertexSpacing,
                    0,
                    //z
                    vertexSpacing * y*(numBlockVertices-1)-(totalMeshVertices / 2) * vertexSpacing),
                    new Quaternion(0, 0, 0, 0)) as GameObject;

                temp.transform.parent = this.transform;


                blocks[x,y] = temp.GetComponent<MeshBlock>();
                blocks[x, y].Initialize(vertexSpacing, numBlockVertices,this);
                
            }
        }


    }

    /// <summary>
    /// Sets the values of the heightmap to the vertices in each
    /// block. Also used to fix the 
    /// </summary>
    /// <param name="map"></param>
    public void setHeightMap(HeightMap map)
    {
        int count = 0;
        //sets the height map
        for(int y = 0; y < totalMeshVertices; y++)
        {
            
            for (int x = 0; x < totalMeshVertices; x++)
            {
                count++;
                setValue(x, y, map.heightMap[x, y]);
            }
        }



        //mends the seams. the seam is the average value of either side of it.
        //the seam consists of 2 vertices at the same height.
        for (int x = numBlockVertices; x < totalMeshVertices; x += numBlockVertices)
        {
            for (int y = 0; y < totalMeshVertices; y++)
            {
                //this is averaging either side of the join and then making the join the average value
                // so that the gradient may be maintained.
                float height = (getHeightFromIndex(x-2, y) + getHeightFromIndex(x+1, y)) / 2f;
                setValue(x, y, height);
                setValue(x-1, y, height);
            }
        }

        for (int y = numBlockVertices; y < totalMeshVertices; y += numBlockVertices)
        {
            for (int x = 0; x < totalMeshVertices; x++)
            {
                //this is averaging either side of the join and then making the join the average value
                // so that the gradient may be maintained.
                float height = (getHeightFromIndex(x, y - 2) + getHeightFromIndex(x, y + 1)) / 2f;
                setValue(x, y, height);
                setValue(x, y-1, height);
            }
            
        }
        
    }





    /// <summary>
    /// sets the value in  the correct mesh block
    /// Vertices array
    /// as if it was one single index'able array.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="val"></param>
    public void setValue(int x, int y, float val)
    {
        int modx = x % numBlockVertices;
        int mody = y % numBlockVertices;

        int indexX = x / numBlockVertices;
        int indexY = y / numBlockVertices;

        blocks[indexX, indexY].setVertexHeight(mody, modx, val);

    }

    /// <summary>
    /// sets the value in the correct mesh block of the texture
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="col"></param>
    public void setTexture(int x, int y, Color col)
    {
        int modx = x % numBlockVertices;
        int mody = y % numBlockVertices;

        int indexX = x / numBlockVertices;
        int indexY = y / numBlockVertices;

        blocks[indexX, indexY].texture.SetPixel(mody, modx, col);

    }

    /// <summary>
    /// gets the height of any xy index from the height map
    /// NOTE: this is NOT the x,z coordinate. i.e it is not 
    /// a coordinate to height converter.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public float getHeightFromIndex(int x, int y)
    {
        int modx = x % numBlockVertices;
        int mody = y % numBlockVertices;

        int indexX = x / numBlockVertices;
        int indexY = y / numBlockVertices;

        //Debug.Log("height = " + blocks[indexX, indexY].getVertexHeight(modx, mody));
        return blocks[indexX, indexY].getVertexHeight(mody, modx);
    }


    /// <summary>
    /// Sets the normal of any given vertex
    /// NOTE : the apply normals must be run after this for changes to be visible
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="val"></param>
    public void setNormal(int x, int y, Vector3 val)
    {
        int modx = x % numBlockVertices;
        int mody = y % numBlockVertices;

        int indexX = x / numBlockVertices;
        int indexY = y / numBlockVertices;


        blocks[indexX, indexY].setNormalHeight(mody, modx, val);
    }

    public Vector3 getNormalFromIndex(int x, int y)
    {
        int modx = x % numBlockVertices;
        int mody = y % numBlockVertices;

        int indexX = x / numBlockVertices;
        int indexY = y / numBlockVertices;

        //Debug.Log("height = " + blocks[indexX, indexY].getVertexHeight(modx, mody));
        return blocks[indexX, indexY].getNormal(mody, modx);
    }





    /// <summary>
    /// passes relevant data to the grid system
    /// </summary>
    public void generateNodes()
    {
        grid.generateMap(heightmap, totalMeshVertices,numBlocks, vertexSpacing);
    }




    public Vector3 getMinBound()
    {
        return blocks[0, 0].transform.position;
    }

    public Vector3 getMaxBound()
    {
        
        return blocks[numBlocks-1, numBlocks-1].transform.position 
            + new Vector3(
                vertexSpacing*numBlockVertices + (numBlocks-1)*vertexSpacing,
                0,
                vertexSpacing*numBlockVertices + (numBlocks - 1) * vertexSpacing);
    }

    /// <summary>
    /// Returns the node closeset to a world position. 
    /// Use for snapping buildings etc.
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public Node nodeFromWorld(Vector3 position)
    {
        return grid.NodeFromWorldPoint(position);
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public float HeightFromWorld(Vector3 position)
    {
        RaycastHit hit;

        if (Physics.Raycast(new Ray(position + new Vector3(0,100,0), Vector3.down), out hit))
        {
            return hit.point.y;
        }


        return -1f;

    }

    public float getMapMax()
    {
        return mapMax;
    }

    public float getMapMin()
    {
        return mapMin;
    }

    public float getWaterHeight()
    {
        return water.transform.position.y;
    }

    



}


