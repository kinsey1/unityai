﻿using UnityEngine;
using System.Collections;

public class Water : MonoBehaviour {

    public Vector3[] Vertices;
    private Vector2[] UV;
    private int[] Triangles;
    private Vector3[] normals;
    Mesh mesh;
    public Texture2D texture;
    public MeshFilter meshFilter;
    //public MeshCollider meshCollider;
    public Renderer meshRenderer;
    public Material material;
    
    public float spacing;

    public int
        length;
    private int 
        triIdx;
    public float amplitude;
    public float period;
    public float rand;

    public MultiMeshManager manager;

    public bool loaded;
    public float scale;
    public float amplitudeSin;

    public void Initialize()
    {
        loaded = false;
        Vector3 pos = new Vector3(-Game.mapMax, 196, -Game.mapMax);
        this.transform.position = pos;
        triIdx = 0;
        texture = new Texture2D(length, length);
        Vertices = new Vector3[length * length];
        UV = new Vector2[Vertices.Length];
        Triangles = new int[(length - 1) * (length - 1) * 6];


        int idx = 0;

        //Setting vertices, triangles and UV array
        for (int x = 0; x < length; x++)
        {
            for (int y = 0; y < length; y++)
            {


                if (y < length - 1 && x < length - 1)
                {
                    genTri(idx, idx + length + 1, idx + length);
                    genTri(idx + length + 1, idx, idx + 1);
                }

                //UV[idx] = new Vector2(x / ((float)length), y / ((float)length));
                
                Vertices[idx] = new Vector3(x * spacing, 0, y * spacing);
                
                idx++;

            }
        }

        updateMesh();
    }

    public void updateMesh()
    {
        mesh = new Mesh();

        mesh.vertices = Vertices;
        mesh.triangles = Triangles;
        //mesh.uv = UV;

        //meshCollider.sharedMesh = mesh;
        //meshRenderer.material.mainTexture = texture;
        meshRenderer.material = material;
        meshFilter.sharedMesh = mesh;
        mesh.RecalculateNormals();
        normals = meshFilter.sharedMesh.normals;
        loaded = true;
    }


    void Update()
    {
        if (loaded)
        {
            
            Vector3[] vertices = meshFilter.sharedMesh.vertices;
            //vertices[0].y += 0.01f;

            //Debug.Log(amplitude * Mathf.Sin(Time.time));


            for (int i = 0; i < vertices.Length; i++)
            {
                float pX = (vertices[i].x * scale) + (Time.timeSinceLevelLoad * period)+rand;
                float pZ = (vertices[i].z * scale) + (Time.timeSinceLevelLoad * period)+rand;

                vertices[i].y = (Mathf.PerlinNoise(pX, pZ) - 0.5f) * amplitude;
                    //Mathf.Sin(Time.time + vertices[i].x + vertices[i].y + vertices[i].z) * amplitudeSin;
            }
            mesh.vertices = vertices;
        }
        

    }

    /// <summary>
    /// generates a triangle based off indexes. v1,v2,v3 are 
    /// indexes from the vertices array
    /// </summary>
    /// <param name="v1"></param>
    /// <param name="v2"></param>
    /// <param name="v3"></param>
    private void genTri(int v1, int v2, int v3)
    {
        Triangles[triIdx] = v1;
        Triangles[triIdx + 1] = v2;
        Triangles[triIdx + 2] = v3;
        triIdx += 3;
    }



}
