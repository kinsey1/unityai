﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid : MonoBehaviour {

	public bool onlyDisplayPathGizmos;
	public LayerMask unwalkableMask;
	int gridWorldSize;
	public float nodeRadius;
    float multiplier;

    private float vertexSpacing;
    private float spacing;

    Node[,] grid;
    //public MeshGenerator meshGen;

	float nodeDiameter;
	int gridSizeX, gridSizeY;
    HeightMap heightMap;

    public List<Node> path;

    int gridSize;


    public int MaxSize {
        get {
            return gridSizeX * gridSizeY;
        }
    }



    


	public void generateMap(HeightMap map, int gridSize, int numBlocks, float vertexSpacing) {

        this.vertexSpacing = vertexSpacing;
        nodeDiameter = nodeRadius*2;
        spacing = (float)(gridSize - numBlocks) / (float)gridSize;
        gridWorldSize = gridSize;

		gridSizeX = Mathf.RoundToInt(((gridWorldSize*vertexSpacing)/nodeDiameter));
		gridSizeY = Mathf.RoundToInt(((gridWorldSize*vertexSpacing) / nodeDiameter));
		

        //NEW HEIGHTMAP
        heightMap = map;

        multiplier = ((float) heightMap.size / (float) gridSizeX);

        grid = new Node[gridSizeX, gridSizeY];

        
        
        //Vector3 worldBottomLeft = new Vector3(gridWorldSize / 2, 0, gridWorldSize / 2);


        //creating grid

        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {

                //Vector3 worldPoint = (Vector3.right * (x * nodeDiameter + nodeRadius) * spacing) + (Vector3.forward * (y * nodeDiameter + nodeRadius) * spacing) ;
                Vector3 worldPoint = (Vector3.right * (x * nodeDiameter + nodeRadius) * spacing) + (Vector3.forward * (y * nodeDiameter + nodeRadius) * spacing) + new Vector3(-(gridWorldSize / 2) * vertexSpacing, 0, -(gridWorldSize / 2) * vertexSpacing);

                worldPoint.y = getHeightMapHeight(x, y) + 2f;

                bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask));
                grid[x, y] = new Node(walkable, worldPoint, x, y);
            }
        }
    }

	
	void CreateGrid() {

		
	}

	public List<Node> GetNeighbours(Node node) {
		List<Node> neighbours = new List<Node>();

		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				if (x == 0 && y == 0)
					continue;

				int checkX = node.gridX + x;
				int checkY = node.gridY + y;

				if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY) {
					neighbours.Add(grid[checkX,checkY]);
				}
			}
		}

		return neighbours;
	}
	

	public Node NodeFromWorldPoint(Vector3 worldPosition) {


        //float percentX = (worldPosition.x + gridWorldSize / 2) / gridWorldSize;
        //float percentY = (worldPosition.z + gridWorldSize / 2) / gridWorldSize;

        float percentX = (worldPosition.x/vertexSpacing + gridWorldSize/2) / gridWorldSize;
		float percentY = (worldPosition.z/vertexSpacing + gridWorldSize/2)/ gridWorldSize;
		percentX = Mathf.Clamp01(percentX);
		percentY = Mathf.Clamp01(percentY);


		int x = Mathf.RoundToInt(((gridSizeX-1) * percentX));
		int y = Mathf.RoundToInt(((gridSizeY-1) * percentY));
		return grid[x,y];
	}

	



	void OnDrawGizmos() {
        //Gizmos.DrawWireCube(transform.position + new Vector3(gridWorldSize/2, 0 , gridWorldSize/2),new Vector3(gridWorldSize, 1,gridWorldSize));
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize*vertexSpacing, 1, gridWorldSize*vertexSpacing));
        //Debug.Log("here11");
        if (onlyDisplayPathGizmos) {
			if (path != null) {
				foreach (Node n in path) {
					Gizmos.color = Color.black;
                    //Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter-.1f));
                    //Gizmos.DrawSphere(n.worldPosition + new Vector3(-gridWorldSize*vertexSpacing, 0, gridWorldSize*vertexSpacing), .2f);
				}
			}
		}
		else {

			if (grid != null) {
				foreach (Node n in grid) {
					Gizmos.color = (n.walkable)?Color.white:Color.red;
					if (path != null)
						if (path.Contains(n))
							Gizmos.color = Color.black;
					//Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter-.1f));
                    Gizmos.DrawSphere(n.worldPosition, .2f);
                }
			}
		}
	}

    //takes the x and y coords of the astar node and gets a height from the height map

    public float getHeightMapHeight(int x, int y)
    {

        int xn = Mathf.RoundToInt((multiplier * (x)));
        int yn = Mathf.RoundToInt((multiplier * (y)));

        return heightMap.heightMap[xn, yn];
    }







}