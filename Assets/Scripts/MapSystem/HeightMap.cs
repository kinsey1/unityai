﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HeightMap
{

    public int size;

    private Texture2D texture;
    public float[,] heightMap;
    public int xOrg, yOrg;
    private float scalar;
    private float freq;
    private int numOctaves;
    private float redistribution;
    private bool island;
    private Vector2 center;
    private float islB, islPow;

    private float minHeight, maxHeight;


    /// <summary>
    /// Creates a heightmap
    /// </summary>
    /// <param name="size">size of the heightmap, will be the same as the mesh size</param>
    /// <param name="scalar">y scalar / jaggedness</param>
    /// <param name="freq">frequency of waves</param>
    /// <param name="numOctaves">number of overlays of increasingly smaller values, more octaves makes more jagged</param>
    /// <param name="redis">redistribution power, good at 1</param>
    /// <param name="island">island</param>
    /// 
    public HeightMap(int size, float scalar, float freq, int numOctaves, float redis, bool island, float islPow, float islB, bool ds)
    {
        //use the diamond square implementation
        if (ds)
        {

        }
        xOrg = 2;
        yOrg = 2;
        heightMap = new float[size, size];
        texture = new Texture2D(size, size);
        this.size = size;
        this.scalar = scalar;
        this.freq = freq;
        this.numOctaves = numOctaves;
        this.redistribution = redis;
        this.island = island;
        this.islPow = islPow;
        this.islB = islB;
        center = new Vector2(size / 2, size / 2);
        generateHeightMap();
    }


    public void generateHeightMap()
    {
        maxHeight = 1;
        minHeight = 1;


        float halfVal = Mathf.Pow(size, 2) / 2;
        //Debug.Log(numOctaves);
        for (int y = 0; y < size; y++)
        {
            for (int x = 0; x < size; x++)
            {
                
                float xCoord = freq*((float)(x) / size);
                float yCoord = freq*((float)(y) / size);

                float sample = 0; 
                

                //adds number of octaves and divides through a number
                //this is where the height map creation happens
                for (int i = 1; i <= numOctaves; i++)
                {
                    sample += (float)(1 / (float)i) * Mathf.PerlinNoise(xCoord * Mathf.Pow(2, i), yCoord * Mathf.Pow(2, i));
                }

                if (island)
                {
                    float dist = Mathf.Abs(Mathf.Pow(center.x - x, 2) + Mathf.Pow(center.y - y, 2));
                    float invDist = islB*(1- Mathf.InverseLerp(0, halfVal, dist));
                    heightMap[x, y] = Mathf.Pow((sample) * scalar, redistribution)*Mathf.Pow(invDist,islPow);
                }else
                {
                    heightMap[x, y] = Mathf.Pow((sample) * scalar, redistribution);
                }


                if (heightMap[x, y] < minHeight)
                    minHeight = heightMap[x, y];
                if (heightMap[x, y] > maxHeight)
                    maxHeight = heightMap[x, y];

                texture.SetPixel(x, y, new Color(0, 1, 0));
            }
        }
    }

    public float[,] getHeightMap()
    {
        return heightMap;
    }

    public Texture2D getTexture()
    {
        
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                texture.SetPixel(x, y, getTexColor(heightMap[x,y]));
            }
        }
        
        return texture;
    }

    public Color getTexColor(float x)
    {
        return Color.red;
    }


    public float getMaxHeight()
    {
        return maxHeight;
    }

    public float getMinHeight()
    {
        return minHeight;
    }
    



}


