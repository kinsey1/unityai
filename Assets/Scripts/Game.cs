﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Provides critical functions and variables that are needed in all aspects of the game.
/// WARNING: this is a global class for all variables. They should only be set using getters/setters.
/// </summary>
[System.Serializable]
public class Game {

    public static MultiMeshManager meshManager;
    public static MonsterManager monsterManager;
    public static WorldManager worldManager;


    public static Game game = new Game();

    public static int length { get; set; }
    public static int mapMin { get; set; }
    public static float mapMax { get { return meshManager.getMapMax(); } }
    public static float vertexSpacing { get { return meshManager.vertexSpacing; } }
    public static float waterHeight {get {return meshManager.getWaterHeight();}}


    public static Game get()
    {
        return game;
    }


    public static float getHeightFromWorld(Vector3 position)
    {
        return meshManager.HeightFromWorld(position);
    }
    public static float getHeightFromIndex(int x, int y)
    {
        return meshManager.getHeightFromIndex(x, y);
    }

    public static Vector3 getRandomMapLocation()
    {
        return worldManager.getRandomMapLocation();
    }




}
