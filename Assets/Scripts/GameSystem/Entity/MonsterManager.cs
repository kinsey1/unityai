﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class MonsterManager : MonoBehaviour
{
    public List<Monster> monsters;

    public int numMonsters;

    public GameObject monster;

    public void Initialize()
    {
        Game.monsterManager = this;
        spawnRandom();
    }

    public void spawnCloseTo(Vector2[] points)
    {
        throw new NotImplementedException();
    }

    public void spawnRandom()
    {
        for (int i =0; i < numMonsters; i++)
        {

            GameObject tempObj;
            Vector3 location = Game.getRandomMapLocation();
            location.y += 2;

            tempObj = Instantiate(monster, location, new Quaternion()) as GameObject;
            
            monsters.Add(tempObj.GetComponent<Monster>());
            tempObj.transform.parent = this.transform;



        }
    }


    public void getSpawnPool()
    {

    }
}
