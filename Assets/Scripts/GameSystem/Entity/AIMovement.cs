﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AIMovement : MonoBehaviour {


    private enum Status
    {
        Attacking,
        Following,
        RandomWalk,
        Waiting,
        Searching
    };

    private GameObject nodeMap;
    private Pathfinding pather;
    private CharacterController controller;
    public float speed;

    Status status;

    //public GameObject target;

    List<Node> path;
    public int gravity;



    void Start () {
        status = Status.RandomWalk;
        controller = GetComponent<CharacterController>();
        nodeMap = GameObject.Find("NodeMap");
        pather = nodeMap.GetComponent<Pathfinding>();
        //getNewPath();

	
	}


    
	
	void Update () {
        
        //followPath();
	}



    public void moveRandom()
    {

    }


    public void getNewPath(GameObject target)
    {
        path = pather.FindPath(transform.position, target.transform.position);
    }

    void followPath()
    {
     

        if (path.Count > 0)
        {
            moveTowards(path[0].worldPosition);

            if (Vector3.Distance(path[0].worldPosition, transform.position) < 20)
            {
                path.RemoveAt(0);
            }

        }
    }

    public void moveTowards(Vector3 target)
    {
        Vector3 direction = target - transform.position;
        direction.y = 0;
        if (!controller.isGrounded)
        {
            controller.Move(new Vector3(0, -1 * gravity * Time.deltaTime));
        }

        controller.Move(direction.normalized * speed * Time.deltaTime);
        //transform.position += direction.normalized * speed * Time.deltaTime;

    }
}



