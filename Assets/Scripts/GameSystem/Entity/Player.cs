﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    public float speed;
    Rigidbody rigid;
    public MultiMeshManager meshManager;
    public GameObject gun;


    // Use this for initialization
    void Start () {
        rigid = GetComponent<Rigidbody>();
        
	}
	


	// Update is called once per frame
	void Update () {


        pcRotation();
        pcMovement();

        meshManager.HeightFromWorld(transform.position);
        
    }

    public void updateHeight()
    {
        
    }


    public void pcRotation()
    {
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);
        
        //Get the Screen position of the mouse
        Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);

        Ray mousePosWorld = Camera.main.ScreenPointToRay(Input.mousePosition);

        float lookAngle;
        RaycastHit hit;

        //Debug.Log(mousePosWorld.direction);

        if (Physics.Raycast(mousePosWorld, out hit))
        {
            lookAngle = AngleBetweenTwoPoints(transform.position, hit.point);
        }else
        {
            lookAngle = 0f;
        }
        float rotAngle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);
        //gun.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f) + transform.rotation.eulerAngles);

        transform.rotation = Quaternion.Euler(new Vector3(0f, -rotAngle, 0f));
        //transform.LookAt(hit.point);
        gun.transform.LookAt(hit.point);
        //transform.rotation.SetLookRotation(Vector3.Normalize(transform.position - hit.point));
    }


    public void pcMovement()
    {
        
        float x = 0, z = 0;
        if (Input.GetKey(KeyCode.A))
            x -= 1;

        if (Input.GetKey(KeyCode.W))
            z += 1;

        if (Input.GetKey(KeyCode.S))
            z -= 1;

        if (Input.GetKey(KeyCode.D))
            x += 1;

        //prevents sliding
        if(x==0 && z == 0)
        {
            Vector3 temp = rigid.velocity;
            temp.x *= 0;
            temp.z *= 0;
            rigid.velocity = temp;

        }

        Vector3 direction = new Vector3(x, 0, z);
        
        this.transform.position += direction * speed * Time.deltaTime;
    }



    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }
}
