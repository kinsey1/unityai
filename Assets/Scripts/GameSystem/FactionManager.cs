﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Contains all relevant data of the factions.
/// </summary>
public class FactionManager : MonoBehaviour {

    public float factionDistance;
    public float borderPadding;
    public int factionRadius;

    public GameObject factionObject;

    public int numFactions;

    private List<Faction> factions;


    public void Initialize()
    {
        generateFactions();
    }


    /// <summary>
    /// generates all the factions in the game based off the numfactions.
    /// in a sense its just pointing locations on the map which are free 
    /// and will have a town around it.
    /// NOTE : Does not generate alliances yet.
    /// </summary>
    public void generateFactions()
    {


        factions = new List<Faction>();

        for (int i = 0; i < numFactions; i++)
        {
            GameObject tempObj;

            Vector3 location = getFreeFactionLocation();

            tempObj = Instantiate(factionObject, location, new Quaternion()) as GameObject;
            tempObj.GetComponent<Faction>().Initialize(location, i, 5, factionRadius, factionObject);

            factions.Add(tempObj.GetComponent<Faction>());
            tempObj.transform.parent = this.transform;

        }




    }


    /// <summary>
    /// gets a free location on map away from other factions
    /// </summary>
    /// <returns></returns>
    public Vector3 getFreeFactionLocation()
    {
        Vector3 location = getRandMapValue();
        if (factions.Count == 0)
        {
            while (nearEdge(location))
            {
                location = getRandMapValue();
            }

            return location;
        }

        bool found = false;

        int i = 0;
        while (found == false)
        {
            if (Vector3.Distance(factions[i].centralPoint, location) < factionDistance)
            {
                i = 0;
                location = getRandMapValue();
                continue;
            }
            if (nearEdge(location))
            {
                location = getRandMapValue();
                i = 0;
                continue;
            }

            if (i == factions.Count - 1)
                break;
            i++;
        }
        Debug.Log(location);

        return location;
    }


    public Vector3 getRandMapValue()
    {
        Vector3 location = new Vector3();


        int x = (int)Random.Range(0, (Game.mapMax * 2 / Game.vertexSpacing));
        int z = (int)Random.Range(0, (Game.mapMax * 2 / Game.vertexSpacing));


        float y = Game.getHeightFromIndex(x, z);



        location.x = x * Game.vertexSpacing - Game.mapMax;
        location.y = y;
        location.z = z * Game.vertexSpacing - Game.mapMax;

        //Debug.Log(location);
        return location;
    }


    void OnDrawGizmos()
    {
        if (factions != null && factions.Count != 0)
        {
            foreach (Faction fac in factions)
            {
                Gizmos.color = Color.blue;
                if (nearEdge(fac.centralPoint))
                    Gizmos.color = Color.red;
                Gizmos.DrawCube(fac.centralPoint, new Vector3(10, 10, 10));
            }
        }

    }


    bool nearEdge(Vector3 position)
    {
        if ((Game.mapMax - Mathf.Abs(position.x) < borderPadding)
            || (Game.mapMax - Mathf.Abs(position.z) < borderPadding))
        {
            return true;
        }



        return false;
    }

}
