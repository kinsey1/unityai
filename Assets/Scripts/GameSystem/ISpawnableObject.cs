﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Interface for all objects which are randomly spawned throughout the map. This excludes towns. 
/// e.g. npcs, trees, items, 
/// </summary>
interface ISpawnableObject {

    /// <summary>
    /// Spawns this object randomly around the map.
    /// </summary>
    void spawnRandom(int numObject);

    /// <summary>
    /// Points at which the object will spawn around
    /// </summary>
    /// <param name="numPoints"></param>
    List<Vector3> genSpawnPoints(int numPoints);

    void spawnCloseTo(Vector2[] points);

    void getSpawnPool();




}
