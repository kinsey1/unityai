﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// Class which is used to set up the world. NPCs, towns, trees, etc.
/// 
/// </summary>
public class WorldManager : MonoBehaviour {

    public MultiMeshManager meshManager;


    public MonsterManager monsterManager;
    public EnvironmentSpawner environmentSpawner;
    public FactionManager factionManager;


    


    public int mapMin, mapMax;
    

    public void Initialize()
    {
        Game.worldManager = this;

        
        mapMax = (int)meshManager.getMaxBound().x;
        mapMin = (int)meshManager.getMinBound().x;

        //Lower level inits
        factionManager.Initialize();
        monsterManager.Initialize();
        environmentSpawner.Initialize();

    }

    //gets a random point on the map.
    public Vector3 getRandomMapLocation()
    {
        float x = Random.Range(mapMin, mapMax);
        float z = Random.Range(mapMin, mapMax);

        return new Vector3(x, Game.getHeightFromWorld(new Vector3(x, 200, z)), z);
    }




    


}
