﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Faction : MonoBehaviour{

    public int population;
    public Vector3 centralPoint;
    private int numStartBuildings;
    private float radius;
    public GameObject watchTower;
    private Vector3[] corners;





    public void Initialize(Vector3 centralPoint, int numAI, int numStartBuildings, int radius, GameObject faction)
    {
        this.centralPoint = centralPoint;
        this.numStartBuildings = numStartBuildings;
        this.radius = radius;

        //the 4 corners of the settlement.
        corners = new Vector3[4];
        generateTownCorners();
    }


    public void generateTownCorners()
    {
        corners[0] = centralPoint + new Vector3(-radius, 50, radius);
        corners[0].y = Game.getHeightFromWorld(corners[0]);
        

        corners[1] = centralPoint + new Vector3(radius, 50, radius);
        corners[1].y = Game.getHeightFromWorld(corners[1]);

        corners[2] = centralPoint + new Vector3(-radius, 50, -radius);
        corners[2].y = Game.getHeightFromWorld(corners[2]);

        corners[3] = centralPoint + new Vector3(radius, 50, -radius);
        corners[3].y = Game.getHeightFromWorld(corners[3]);


        for (int i =0; i < 4; i++)
        {
            GameObject temp = Instantiate(watchTower, corners[i], new Quaternion(0,0,0,0)) as GameObject;
            temp.transform.parent = this.transform;
        }
    }

    public void generateBuildings()
    {
        for (int i= 0; i < numStartBuildings; i++)
        {

        }
    }


    


    void getRandomRadialPoint(float distance, Vector3 center)
    {
        

    }

}
