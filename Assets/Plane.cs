﻿using UnityEngine;
using System.Collections;

public class Plane : MonoBehaviour {

    public Vector3[] Vertices;
    private Vector2[] UV;
    private int[] Triangles;

    public Vector3[] triVertices;
    public int[] newTriangles;

    private Vector3[] normals;
    Mesh mesh;

    public MeshFilter meshFilter;
    public Renderer meshRenderer;


    public int
        length;
    private int
        triIdx;
    public float spacing;

    public Color32[] colorsPicker;
    private Color32[] colors;
    public GameObject pointLight;
    public Shader shader;

    // Use this for initialization
    void Start () {
        Initialize();
	}

    void Update()
    {
        meshRenderer.material.SetColor("_PointLightColor", pointLight.GetComponent<Light>().color);
        meshRenderer.material.SetVector("_PointLightPosition", pointLight.transform.position);
    }


    public void Initialize()
    {

        meshRenderer.material.shader = shader;
        triIdx = 0;
        Vertices = new Vector3[length*length];
        colors = new Color32[(length - 1) * (length - 1) * 6];
        Triangles = new int[(length - 1) * (length - 1) * 6];

        triVertices = new Vector3[(length - 1) * (length - 1) * 6];
        newTriangles = new int[(length - 1) * (length - 1) * 6];

        int idx = 0;

        //Setting vertices, triangles and UV array
        for (int x = 0; x < length; x++)
        {
            for (int y = 0; y < length; y++)
            {

                Vertices[idx] = new Vector3(x * spacing, 0, y * spacing);

                if (y < length - 1 && x < length - 1)
                {
                    genTri(idx, idx + length + 1, idx + length);
                    genTri(idx + length + 1, idx, idx + 1);
                }
                idx++;

            }
        }
        //unique vertices
        Color32 curr;
        curr = colorsPicker[Random.Range(0, 3)];
        for (int i = 0; i < triVertices.Length; i++)
        {
            triVertices[i] = Vertices[Triangles[i]];
            newTriangles[i] = i; colors[i] = colorsPicker[Random.Range(0, 2)];
            if (i % 3 == 0)
            {
                curr = colorsPicker[Random.Range(0, 3)];
            }
            colors[i] = curr;
            //colors[i] = new Color32(255, 255, 255, 255);

        }
        updateMesh();
    }

    public void updateMesh()
    {


        mesh = new Mesh();

        mesh.vertices = triVertices;
        mesh.triangles = newTriangles;


        //meshCollider.sharedMesh = mesh;
        //meshRenderer.material.mainTexture = texture;
        meshFilter.sharedMesh = mesh;
        mesh.RecalculateNormals();
        normals = meshFilter.sharedMesh.normals;
        mesh.colors32 = colors;

        Debug.Log(Vertices.Length);
        Debug.Log(triVertices.Length);
        for(int i = 0; i < triVertices.Length; i++)
        {
            Debug.Log(triVertices[i]);
        }
    }



    /// <summary>
    /// generates a triangle based off indexes. v1,v2,v3 are 
    /// indexes from the vertices array
    /// </summary>
    /// <param name="v1"></param>
    /// <param name="v2"></param>
    /// <param name="v3"></param>
    private void genTri(int v1, int v2, int v3)
    {
        Triangles[triIdx] = v1;
        Triangles[triIdx + 1] = v2;
        Triangles[triIdx + 2] = v3;
        triIdx += 3;
    }
}



